﻿import QtQuick 2.0
IGuiPage
{
	id: q16777220
	objId: 16777220
	x: 0
	y: 0
	width: 800
	height: 480
	IGuiIOField
	{
		id: q33554455
		objId: 33554455
		x: 270
		y: 79
		width: 106
		height: 42
		qm_FillRectWidth: 106
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435487
		objId: 268435487
		x: 2
		y: 89
		width: 247
		height: 25
		qm_FillRectWidth: 247
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiIOField
	{
		id: q33554456
		objId: 33554456
		x: 270
		y: 132
		width: 106
		height: 42
		qm_FillRectWidth: 106
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435488
		objId: 268435488
		x: 2
		y: 142
		width: 247
		height: 25
		qm_FillRectWidth: 247
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiIOField
	{
		id: q33554457
		objId: 33554457
		x: 270
		y: 185
		width: 106
		height: 42
		qm_FillRectWidth: 106
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435489
		objId: 268435489
		x: 2
		y: 195
		width: 247
		height: 25
		qm_FillRectWidth: 247
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiIOField
	{
		id: q33554458
		objId: 33554458
		x: 270
		y: 238
		width: 106
		height: 42
		qm_FillRectWidth: 106
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435490
		objId: 268435490
		x: 2
		y: 248
		width: 247
		height: 25
		qm_FillRectWidth: 247
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiIOField
	{
		id: q33554459
		objId: 33554459
		x: 270
		y: 291
		width: 106
		height: 42
		qm_FillRectWidth: 106
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435491
		objId: 268435491
		x: 2
		y: 301
		width: 247
		height: 25
		qm_FillRectWidth: 247
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiIOField
	{
		id: q33554460
		objId: 33554460
		x: 513
		y: 79
		width: 106
		height: 42
		qm_FillRectWidth: 106
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435492
		objId: 268435492
		x: 390
		y: 89
		width: 114
		height: 25
		qm_FillRectWidth: 114
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiIOField
	{
		id: q33554461
		objId: 33554461
		x: 513
		y: 132
		width: 106
		height: 42
		qm_FillRectWidth: 106
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiIOField
	{
		id: q33554462
		objId: 33554462
		x: 513
		y: 185
		width: 106
		height: 42
		qm_FillRectWidth: 106
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiIOField
	{
		id: q33554463
		objId: 33554463
		x: 513
		y: 238
		width: 106
		height: 42
		qm_FillRectWidth: 106
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiIOField
	{
		id: q33554464
		objId: 33554464
		x: 513
		y: 291
		width: 106
		height: 42
		qm_FillRectWidth: 106
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435493
		objId: 268435493
		x: 624
		y: 88
		width: 66
		height: 25
		qm_FillRectWidth: 66
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435494
		objId: 268435494
		x: 390
		y: 142
		width: 114
		height: 25
		qm_FillRectWidth: 114
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435495
		objId: 268435495
		x: 390
		y: 195
		width: 114
		height: 25
		qm_FillRectWidth: 114
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435496
		objId: 268435496
		x: 390
		y: 248
		width: 114
		height: 25
		qm_FillRectWidth: 114
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435497
		objId: 268435497
		x: 390
		y: 301
		width: 114
		height: 25
		qm_FillRectWidth: 114
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435498
		objId: 268435498
		x: 376
		y: 89
		width: 34
		height: 25
		qm_FillRectWidth: 34
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435499
		objId: 268435499
		x: 376
		y: 142
		width: 34
		height: 25
		qm_FillRectWidth: 34
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435500
		objId: 268435500
		x: 376
		y: 195
		width: 34
		height: 25
		qm_FillRectWidth: 34
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435501
		objId: 268435501
		x: 376
		y: 248
		width: 34
		height: 25
		qm_FillRectWidth: 34
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435502
		objId: 268435502
		x: 376
		y: 301
		width: 34
		height: 25
		qm_FillRectWidth: 34
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiGraphicButton
	{
		id: q486539307
		objId: 486539307
		x: 65
		y: 85
		width: 41
		height: 34
		qm_FillRectWidth: 40
		qm_FillRectHeight: 33
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_BorderColor: "#ff424952"
		qm_ImageID: 67
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ff636573"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_Streached :true
		qm_GraphicImageID :65
		qm_ContentVisibility : false
	}
	IGuiGraphicButton
	{
		id: q486539308
		objId: 486539308
		x: 65
		y: 135
		width: 41
		height: 34
		qm_FillRectWidth: 40
		qm_FillRectHeight: 33
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_BorderColor: "#ff424952"
		qm_ImageID: 67
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ff636573"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_Streached :true
		qm_GraphicImageID :65
		qm_ContentVisibility : false
	}
	IGuiGraphicButton
	{
		id: q486539309
		objId: 486539309
		x: 65
		y: 193
		width: 41
		height: 34
		qm_FillRectWidth: 40
		qm_FillRectHeight: 33
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_BorderColor: "#ff424952"
		qm_ImageID: 67
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ff636573"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_Streached :true
		qm_GraphicImageID :65
		qm_ContentVisibility : false
	}
	IGuiGraphicButton
	{
		id: q486539310
		objId: 486539310
		x: 65
		y: 243
		width: 41
		height: 34
		qm_FillRectWidth: 40
		qm_FillRectHeight: 33
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_BorderColor: "#ff424952"
		qm_ImageID: 67
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ff636573"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_Streached :true
		qm_GraphicImageID :65
		qm_ContentVisibility : false
	}
	IGuiGraphicButton
	{
		id: q486539311
		objId: 486539311
		x: 65
		y: 292
		width: 41
		height: 34
		qm_FillRectWidth: 40
		qm_FillRectHeight: 33
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_BorderColor: "#ff424952"
		qm_ImageID: 67
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ff636573"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_Streached :true
		qm_GraphicImageID :65
		qm_ContentVisibility : false
	}
	IGuiButton
	{
		id: q486539312
		objId: 486539312
		x: 519
		y: 372
		width: 96
		height: 32
		qm_FillRectWidth: 95
		qm_FillRectHeight: 31
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_BorderColor: "#ff424952"
		qm_ImageID: 27
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ff636573"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_FontSize: 16
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
	}
	IGuiTextField
	{
		id: q268435503
		objId: 268435503
		x: 368
		y: 376
		width: 124
		height: 20
		qm_FillRectWidth: 124
		qm_FillRectHeight: 20
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
		qm_FontSize: 15
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 0
		qm_BorderColor: "#ff424952"
		qm_ImageID: 38
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 3
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435504
		objId: 268435504
		x: 294
		y: 36
		width: 173
		height: 27
		qm_FillRectWidth: 173
		qm_FillRectHeight: 27
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
		qm_FontSize: 22
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 0
		qm_BorderColor: "#ff424952"
		qm_ImageID: 38
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 3
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435505
		objId: 268435505
		x: 624
		y: 142
		width: 66
		height: 25
		qm_FillRectWidth: 66
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435506
		objId: 268435506
		x: 624
		y: 196
		width: 66
		height: 25
		qm_FillRectWidth: 66
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435507
		objId: 268435507
		x: 624
		y: 250
		width: 66
		height: 25
		qm_FillRectWidth: 66
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435508
		objId: 268435508
		x: 624
		y: 304
		width: 66
		height: 25
		qm_FillRectWidth: 66
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435512
		objId: 268435512
		x: 434
		y: 414
		width: 158
		height: 20
		qm_FillRectWidth: 158
		qm_FillRectHeight: 20
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
		qm_FontSize: 15
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 0
		qm_BorderColor: "#ff424952"
		qm_ImageID: 38
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 3
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiIOField
	{
		id: q33554467
		objId: 33554467
		x: 254
		y: 375
		width: 40
		height: 30
		qm_FillRectWidth: 40
		qm_FillRectHeight: 30
		qm_FontSize: 15
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435513
		objId: 268435513
		x: 301
		y: 382
		width: 17
		height: 25
		qm_FillRectWidth: 17
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435514
		objId: 268435514
		x: 153
		y: 376
		width: 90
		height: 20
		qm_FillRectWidth: 90
		qm_FillRectHeight: 20
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
		qm_FontSize: 15
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 0
		qm_BorderColor: "#ff424952"
		qm_ImageID: 38
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 3
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
}
