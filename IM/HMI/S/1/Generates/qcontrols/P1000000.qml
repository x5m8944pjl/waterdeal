﻿import QtQuick 2.0
IGuiPage
{
	id: q16777216
	objId: 16777216
	x: 0
	y: 0
	width: 800
	height: 480
	IGuiButton
	{
		id: q486539292
		objId: 486539292
		x: 601
		y: 177
		width: 96
		height: 32
		qm_FillRectWidth: 95
		qm_FillRectHeight: 31
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_BorderColor: "#ff424952"
		qm_ImageID: 27
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ff636573"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_FontSize: 16
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
	}
	IGuiButton
	{
		id: q486539293
		objId: 486539293
		x: 601
		y: 229
		width: 96
		height: 32
		qm_FillRectWidth: 95
		qm_FillRectHeight: 31
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_BorderColor: "#ff424952"
		qm_ImageID: 27
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ff636573"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_FontSize: 16
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
	}
	IGuiIOField
	{
		id: q33554436
		objId: 33554436
		x: 517
		y: 436
		width: 184
		height: 29
		qm_FillRectWidth: 183
		qm_FillRectHeight: 28
		qm_FontSize: 16
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 4
		qm_BorderColor: "#ff424952"
		qm_ImageID: 28
		qm_TileTop: 5
		qm_TileBottom: 5
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ffffffff"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 3
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiGraphicView
	{
		id: q301989889
		objId: 301989889
		x: 139
		y: 2
		width: 343
		height: 478
		qm_FillRectWidth: 343
		qm_FillRectHeight: 478
		qm_GraphicImageID: 29
		imageStreched: true
		qm_TextColor: "#00000000"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_Transparent : true 
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 26
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
	}
	IGuiTextField
	{
		id: q268435463
		objId: 268435463
		x: 144
		y: 156
		width: 31
		height: 21
		qm_FillRectWidth: 31
		qm_FillRectHeight: 21
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 16
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiIOField
	{
		id: q33554437
		objId: 33554437
		x: 74
		y: 156
		width: 70
		height: 27
		qm_FillRectWidth: 70
		qm_FillRectHeight: 27
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 31
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ff00ff00"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiIOField
	{
		id: q33554438
		objId: 33554438
		x: 220
		y: 397
		width: 40
		height: 32
		qm_FillRectWidth: 40
		qm_FillRectHeight: 32
		qm_FontSize: 15
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 31
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ff00ff00"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiIOField
	{
		id: q33554439
		objId: 33554439
		x: 220
		y: 432
		width: 40
		height: 30
		qm_FillRectWidth: 40
		qm_FillRectHeight: 30
		qm_FontSize: 15
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiIOField
	{
		id: q33554440
		objId: 33554440
		x: 245
		y: 326
		width: 64
		height: 26
		qm_FillRectWidth: 64
		qm_FillRectHeight: 26
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 31
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ff00ff00"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435464
		objId: 268435464
		x: 309
		y: 327
		width: 40
		height: 21
		qm_FillRectWidth: 40
		qm_FillRectHeight: 21
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 16
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435465
		objId: 268435465
		x: 113
		y: 324
		width: 31
		height: 21
		qm_FillRectWidth: 31
		qm_FillRectHeight: 21
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 16
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiIOField
	{
		id: q33554441
		objId: 33554441
		x: 49
		y: 322
		width: 64
		height: 26
		qm_FillRectWidth: 64
		qm_FillRectHeight: 26
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 31
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ff00ff00"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiIOField
	{
		id: q33554442
		objId: 33554442
		x: 74
		y: 109
		width: 70
		height: 29
		qm_FillRectWidth: 70
		qm_FillRectHeight: 29
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 31
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ff00ff00"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435466
		objId: 268435466
		x: 144
		y: 113
		width: 21
		height: 21
		qm_FillRectWidth: 21
		qm_FillRectHeight: 21
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 16
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiGraphicButton
	{
		id: q486539294
		objId: 486539294
		x: 282
		y: 399
		width: 119
		height: 65
		qm_FillRectWidth: 119
		qm_FillRectHeight: 65
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 0
		qm_BorderColor: "#ff424952"
		qm_ImageID: 35
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_Streached :true
		qm_GraphicImageID :33
		qm_ContentVisibility : false
	}
	IGuiGraphicButton
	{
		id: q486539295
		objId: 486539295
		x: 144
		y: 194
		width: 77
		height: 129
		qm_FillRectWidth: 77
		qm_FillRectHeight: 129
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 0
		qm_BorderColor: "#ff424952"
		qm_ImageID: 35
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_Streached :true
		qm_GraphicImageID :36
		qm_ContentVisibility : false
	}
	IGuiTextField
	{
		id: q268435467
		objId: 268435467
		x: 260
		y: 401
		width: 17
		height: 25
		qm_FillRectWidth: 17
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435468
		objId: 268435468
		x: 260
		y: 436
		width: 17
		height: 25
		qm_FillRectWidth: 17
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiButton
	{
		id: q486539296
		objId: 486539296
		x: 601
		y: 124
		width: 96
		height: 32
		qm_FillRectWidth: 95
		qm_FillRectHeight: 31
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_BorderColor: "#ff424952"
		qm_ImageID: 27
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ff636573"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_FontSize: 16
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
	}
	IGuiTextField
	{
		id: q268435469
		objId: 268435469
		x: 517
		y: 183
		width: 90
		height: 20
		qm_FillRectWidth: 90
		qm_FillRectHeight: 20
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
		qm_FontSize: 15
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 0
		qm_BorderColor: "#ff424952"
		qm_ImageID: 38
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 3
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435470
		objId: 268435470
		x: 517
		y: 129
		width: 90
		height: 20
		qm_FillRectWidth: 90
		qm_FillRectHeight: 20
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
		qm_FontSize: 15
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 0
		qm_BorderColor: "#ff424952"
		qm_ImageID: 38
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 3
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiGraphicButton
	{
		id: q486539297
		objId: 486539297
		x: 49
		y: 204
		width: 57
		height: 55
		qm_FillRectWidth: 57
		qm_FillRectHeight: 55
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 2
		qm_BorderColor: "#ffe7e3e7"
		qm_ImageID: 41
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ffffffff"
		qm_TextColor: "#ff000000"
		qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 1
		qm_FocusColor: "#ff000000"
		qm_Streached :false
		qm_GraphicImageID :39
		qm_ContentVisibility : false
	}
	IGuiIOField
	{
		id: q33554443
		objId: 33554443
		x: 248
		y: 255
		width: 64
		height: 24
		qm_FillRectWidth: 64
		qm_FillRectHeight: 24
		qm_FontSize: 15
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiIOField
	{
		id: q33554444
		objId: 33554444
		x: 248
		y: 157
		width: 64
		height: 26
		qm_FillRectWidth: 64
		qm_FillRectHeight: 26
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 31
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ff00ff00"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435471
		objId: 268435471
		x: 312
		y: 158
		width: 13
		height: 21
		qm_FillRectWidth: 13
		qm_FillRectHeight: 21
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 16
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiIOField
	{
		id: q33554445
		objId: 33554445
		x: 248
		y: 194
		width: 64
		height: 26
		qm_FillRectWidth: 64
		qm_FillRectHeight: 26
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 31
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ff00ff00"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435472
		objId: 268435472
		x: 312
		y: 195
		width: 13
		height: 21
		qm_FillRectWidth: 13
		qm_FillRectHeight: 21
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 16
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435473
		objId: 268435473
		x: 317
		y: 255
		width: 13
		height: 21
		qm_FillRectWidth: 13
		qm_FillRectHeight: 21
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 16
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435511
		objId: 268435511
		x: 526
		y: 183
		width: 175
		height: 20
		qm_FillRectWidth: 175
		qm_FillRectHeight: 20
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
		qm_FontSize: 15
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 0
		qm_BorderColor: "#ff424952"
		qm_ImageID: 38
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 3
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
}
