﻿import QtQuick 2.0
IGuiPage
{
	id: q16777218
	objId: 16777218
	x: 0
	y: 0
	width: 800
	height: 480
	IGuiIOField
	{
		id: q33554446
		objId: 33554446
		x: 333
		y: 118
		width: 106
		height: 42
		qm_FillRectWidth: 106
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435475
		objId: 268435475
		x: 74
		y: 132
		width: 247
		height: 25
		qm_FillRectWidth: 247
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435476
		objId: 268435476
		x: 548
		y: 94
		width: 30
		height: 17
		qm_FillRectWidth: 30
		qm_FillRectHeight: 17
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 12
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ff00cfff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiIOField
	{
		id: q33554447
		objId: 33554447
		x: 519
		y: 117
		width: 88
		height: 42
		qm_FillRectWidth: 88
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435477
		objId: 268435477
		x: 609
		y: 126
		width: 22
		height: 25
		qm_FillRectWidth: 22
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435478
		objId: 268435478
		x: 89
		y: 176
		width: 246
		height: 21
		qm_FillRectWidth: 246
		qm_FillRectHeight: 21
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 16
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiIOField
	{
		id: q33554448
		objId: 33554448
		x: 333
		y: 165
		width: 106
		height: 42
		qm_FillRectWidth: 106
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiIOField
	{
		id: q33554449
		objId: 33554449
		x: 519
		y: 213
		width: 88
		height: 42
		qm_FillRectWidth: 88
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435479
		objId: 268435479
		x: 607
		y: 221
		width: 25
		height: 25
		qm_FillRectWidth: 25
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435480
		objId: 268435480
		x: 89
		y: 227
		width: 246
		height: 21
		qm_FillRectWidth: 246
		qm_FillRectHeight: 21
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 16
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiIOField
	{
		id: q33554450
		objId: 33554450
		x: 333
		y: 213
		width: 106
		height: 42
		qm_FillRectWidth: 106
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiIOField
	{
		id: q33554451
		objId: 33554451
		x: 334
		y: 260
		width: 105
		height: 42
		qm_FillRectWidth: 105
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435481
		objId: 268435481
		x: 74
		y: 273
		width: 238
		height: 25
		qm_FillRectWidth: 238
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiIOField
	{
		id: q33554452
		objId: 33554452
		x: 333
		y: 308
		width: 106
		height: 42
		qm_FillRectWidth: 106
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435482
		objId: 268435482
		x: 89
		y: 321
		width: 223
		height: 25
		qm_FillRectWidth: 223
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiTextField
	{
		id: q268435483
		objId: 268435483
		x: 84
		y: 369
		width: 228
		height: 25
		qm_FillRectWidth: 228
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiIOField
	{
		id: q33554453
		objId: 33554453
		x: 519
		y: 331
		width: 88
		height: 42
		qm_FillRectWidth: 88
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiIOField
	{
		id: q33554454
		objId: 33554454
		x: 333
		y: 356
		width: 106
		height: 42
		qm_FillRectWidth: 106
		qm_FillRectHeight: 42
		qm_FontSize: 17
		qm_FontFamilyName: "SimSun"
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 1
		qm_BorderColor: "#ffffffff"
		qm_ImageID: 32
		qm_TileTop: 2
		qm_TileBottom: 2
		qm_TileRight: 2
		qm_TileLeft: 2
		qm_FillColor: "#ff000000"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
		qm_passwordMode: false
		qm_FontRendringTpe:1
	}
	IGuiTextField
	{
		id: q268435484
		objId: 268435484
		x: 609
		y: 339
		width: 28
		height: 25
		qm_FillRectWidth: 28
		qm_FillRectHeight: 25
		qm_FontFamilyName: "SimSun"
		qm_FontSize: 17
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 30
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignTop
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 2
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiLine
	{
		id: q671088647
		objId: 671088647
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff181c31"
		qm_LineStartArrow: 0
		qm_LineEndArrow: 0
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: 1
	}
	IGuiLine
	{
		id: q671088648
		objId: 671088648
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff181c31"
		qm_LineStartArrow: 0
		qm_LineEndArrow: 0
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: 1
	}
	IGuiLine
	{
		id: q671088649
		objId: 671088649
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff181c31"
		qm_LineStartArrow: 0
		qm_LineEndArrow: 0
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: 1
	}
	IGuiLine
	{
		id: q671088650
		objId: 671088650
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff181c31"
		qm_LineStartArrow: 0
		qm_LineEndArrow: 0
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: 1
	}
	IGuiLine
	{
		id: q671088651
		objId: 671088651
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff181c31"
		qm_LineStartArrow: 0
		qm_LineEndArrow: 0
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: 1
	}
	IGuiLine
	{
		id: q671088652
		objId: 671088652
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff181c31"
		qm_LineStartArrow: 0
		qm_LineEndArrow: 0
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: 1
	}
	IGuiTextField
	{
		id: q268435485
		objId: 268435485
		x: 321
		y: 43
		width: 149
		height: 27
		qm_FillRectWidth: 149
		qm_FillRectHeight: 27
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
		qm_FontSize: 22
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 0
		qm_BorderColor: "#ff424952"
		qm_ImageID: 38
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 3
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
}
