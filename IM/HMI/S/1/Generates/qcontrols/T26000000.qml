﻿import QtQuick 2.0
IGuiTemplate
{
	id: q637534208
	objId: 637534208
	x: 0
	y: 0
	width: 800
	height: 480
	IGuiGraphicButton
	{
		id: q486539285
		objId: 486539285
		x: 717
		y: 78
		width: 80
		height: 62
		qm_FillRectWidth: 79
		qm_FillRectHeight: 61
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 1
		qm_BorderColor: "#ff9c9aa5"
		qm_ImageID: 18
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_Streached :true
		qm_GraphicImageID :17
		qm_ContentVisibility : false
	}
	IGuiGraphicButton
	{
		id: q486539286
		objId: 486539286
		x: 718
		y: 9
		width: 79
		height: 63
		qm_FillRectWidth: 78
		qm_FillRectHeight: 62
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 1
		qm_BorderColor: "#ff9c9aa5"
		qm_ImageID: 18
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_Streached :true
		qm_GraphicImageID :19
		qm_ContentVisibility : false
	}
	IGuiGraphicButton
	{
		id: q486539287
		objId: 486539287
		x: 718
		y: 343
		width: 80
		height: 60
		qm_FillRectWidth: 79
		qm_FillRectHeight: 59
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 1
		qm_BorderColor: "#ff9c9aa5"
		qm_ImageID: 18
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_Streached :true
		qm_GraphicImageID :20
		qm_ContentVisibility : false
	}
	IGuiGraphicButton
	{
		id: q486539288
		objId: 486539288
		x: 717
		y: 412
		width: 81
		height: 63
		qm_FillRectWidth: 80
		qm_FillRectHeight: 62
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 1
		qm_BorderColor: "#ff9c9aa5"
		qm_ImageID: 18
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_Streached :true
		qm_GraphicImageID :21
		qm_ContentVisibility : false
	}
	IGuiGraphicButton
	{
		id: q486539289
		objId: 486539289
		x: 717
		y: 147
		width: 78
		height: 58
		qm_FillRectWidth: 77
		qm_FillRectHeight: 57
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 1
		qm_BorderColor: "#ff9c9aa5"
		qm_ImageID: 18
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_Streached :true
		qm_GraphicImageID :22
		qm_ContentVisibility : false
	}
	IGuiGraphicButton
	{
		id: q486539290
		objId: 486539290
		x: 717
		y: 213
		width: 78
		height: 58
		qm_FillRectWidth: 77
		qm_FillRectHeight: 57
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 1
		qm_BorderColor: "#ff9c9aa5"
		qm_ImageID: 18
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_Streached :true
		qm_GraphicImageID :23
		qm_ContentVisibility : false
	}
	IGuiGraphicView
	{
		id: q301989888
		objId: 301989888
		x: 3
		y: 3
		width: 107
		height: 41
		qm_FillRectWidth: 107
		qm_FillRectHeight: 41
		qm_GraphicImageID: 25
		imageStreched: true
		qm_TextColor: "#00000000"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_Transparent : true 
		qm_BorderCornerRadius: 0
		qm_BorderWidth: 0
		qm_BorderColor: "#ff000000"
		qm_ImageID: 26
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
	}
	IGuiGraphicButton
	{
		id: q486539291
		objId: 486539291
		x: 718
		y: 278
		width: 78
		height: 58
		qm_FillRectWidth: 77
		qm_FillRectHeight: 57
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 1
		qm_BorderColor: "#ff9c9aa5"
		qm_ImageID: 18
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_Streached :true
		qm_GraphicImageID :24
		qm_ContentVisibility : false
	}
}
