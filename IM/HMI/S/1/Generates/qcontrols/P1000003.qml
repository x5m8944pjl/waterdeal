﻿import QtQuick 2.0
IGuiPage
{
	id: q16777219
	objId: 16777219
	x: 0
	y: 0
	width: 800
	height: 480
	IGuiTrendView
	{
		id: q469762048
		objId: 469762048
		x: 79
		y: 77
		width: 600
		height: 395
		qm_FillRectWidth: 599
		qm_FillRectHeight: 394
		qm_BorderCornerRadius: 4
		qm_BorderWidth: 1
		qm_BorderColor: "#ff636973"
		qm_ImageID: 43
		qm_TileTop: 6
		qm_TileBottom: 6
		qm_TileRight: 6
		qm_TileLeft: 6
		qm_FillColor: "#fff7f3f7"
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		rulerColor: "#7b7d84"
		rulerVisibility: true
		qm_TrendXPos: 2
		qm_TrendYPos: 2
		qm_TrendWidth: 596
		qm_TrendHeight: 260
		IGuiListCtrl
		{
			id: qu469762048
			objectName: "qu469762048"
			x: 6
			y: 308
			width: 588
			height: 81
			totalColumnWidth: 588
			qm_leftImageID: 44
			qm_leftTileTop: 4
			qm_leftTileBottom: 10
			qm_leftTileRight: 2
			qm_leftTileLeft: 4
			qm_middleImageID: 45
			qm_middleTileTop: 2
			qm_middleTileBottom: 12
			qm_middleTileRight: 2
			qm_middleTileLeft: 2
			qm_rightImageID: 46
			qm_rightTileTop: 4
			qm_rightTileBottom: 10
			qm_rightTileRight: 4
			qm_rightTileLeft: 2
			qm_leftBorderCornerRadius: 2
			qm_tableBackColor: "#ffffffff"
			qm_tableSelectBackColor: "#ffd6dfef"
			qm_tableAlternateBackColor: "#ffe7e7ef"
			qm_tableHeaderBackColor: "#ff84868c"
			qm_tableTextColor: "#ff000000"
			qm_tableSelectTextColor: "#ff424952"
			qm_tableAlternateTextColor: "#ff000000"
			qm_tableMarginLeft: 0
			qm_tableMarginRight: 0
			qm_tableMarginBottom: 0
			qm_tableMarginTop: 0
			qm_tableHeaderTextColor: "#ffffffff"
			qm_tableHeaderValueVarTextAlignmentHorizontal: Text.AlignLeft
			qm_tableHeaderValueVarTextAlignmentVertical: Text.AlignVCenter
			qm_tableHeaderValueVarTextOrientation: 0
			qm_tableHeaderMarginLeft: 3
			qm_tableHeaderMarginRight: 1
			qm_tableHeaderMarginBottom: 1
			qm_tableHeaderMarginTop: 1
			qm_gridLineStyle: 0
			qm_gridLineWidth: 1
			qm_gridLineColor: "#ffffffff"
			qm_noOfColumns: 4
			qm_tableRowHeight: 13
			qm_tableHeaderHeight: 12
			qm_hasHeader: true
			qm_hasGridLines: true
			qm_hasBorder: true
			qm_hasDisplayFocusLine: true
			qm_hasVerticalScrolling: true
			qm_hasVerticalScrollBar: true
			qm_hasHorizontalScrollBar: true
			qm_hasColumnOrdering: false
			qm_hasHighLightFullRow: true
			qm_hasVerUpDownPresent: false
			qm_hasVerPgUpDownPresent: false
			qm_hasHighlight: false
			qm_hasUpDownAsPageUpDown: false
			qm_hasLongAlarmButton: false
			qm_hasExtraPixelForLineHeight: false
			qm_hasRowEditable: false
			qm_hasRowJustification: false
			qm_hasRowJustificationBottom: false
			qm_linesPerRow: 1
			IGuiListColumnView
			{
				id: qc11c000000
				colIndex: 0
				x: 0
				y: 0
				width: 172
				height: 49
				columnType: 0
				qm_columnValueVarTextAlignmentHorizontal: Text.AlignLeft
				qm_columnValueVarTextAlignmentVertical: Text.AlignVCenter
			}
			IGuiListColumnView
			{
				id: qc21c000000
				colIndex: 1
				x: 172
				y: 0
				width: 72
				height: 49
				columnType: 0
				qm_columnValueVarTextAlignmentHorizontal: Text.AlignLeft
				qm_columnValueVarTextAlignmentVertical: Text.AlignVCenter
			}
			IGuiListColumnView
			{
				id: qc31c000000
				colIndex: 2
				x: 244
				y: 0
				width: 168
				height: 49
				columnType: 0
				qm_columnValueVarTextAlignmentHorizontal: Text.AlignLeft
				qm_columnValueVarTextAlignmentVertical: Text.AlignVCenter
			}
			IGuiListColumnView
			{
				id: qc41c000000
				colIndex: 3
				x: 412
				y: 0
				width: 176
				height: 49
				columnType: 0
				qm_columnValueVarTextAlignmentHorizontal: Text.AlignLeft
				qm_columnValueVarTextAlignmentVertical: Text.AlignVCenter
			}
			qm_UseRowSpecificColor: true
		}
		IGuiGraphicSwitch
		{
			id: q352321536
			objId: 352321536
			x: 7
			y: 262
			width: 56
			height: 40
			qm_FillRectWidth: 55
			qm_FillRectHeight: 39
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_BorderColor: "#ff9c9aa5"
			qm_ImageID: 47
			qm_TileTop: 15
			qm_TileBottom: 15
			qm_TileRight: 5
			qm_TileLeft: 5
			qm_FillColor: "#ffefebef"
			qm_TextColor: "#00000000"
			qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
			qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
			qm_ValueVarTextOrientation: 0
			qm_MarginLeft: 0
			qm_MarginRight: 0
			qm_MarginBottom: 0
			qm_MarginTop: 0
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_Streached :false
			qm_GraphicImageID :48
		}
		IGuiGraphicButton
		{
			id: q486539299
			objId: 486539299
			x: 68
			y: 262
			width: 56
			height: 40
			qm_FillRectWidth: 55
			qm_FillRectHeight: 39
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_BorderColor: "#ff9c9aa5"
			qm_ImageID: 47
			qm_TileTop: 15
			qm_TileBottom: 15
			qm_TileRight: 5
			qm_TileLeft: 5
			qm_FillColor: "#ffefebef"
			qm_TextColor: "#00000000"
			qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
			qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
			qm_ValueVarTextOrientation: 0
			qm_MarginLeft: 0
			qm_MarginRight: 0
			qm_MarginBottom: 0
			qm_MarginTop: 0
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_Streached :false
			qm_GraphicImageID :50
			qm_ContentVisibility : false
		}
		IGuiGraphicButton
		{
			id: q486539300
			objId: 486539300
			x: 129
			y: 262
			width: 56
			height: 40
			qm_FillRectWidth: 55
			qm_FillRectHeight: 39
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_BorderColor: "#ff9c9aa5"
			qm_ImageID: 47
			qm_TileTop: 15
			qm_TileBottom: 15
			qm_TileRight: 5
			qm_TileLeft: 5
			qm_FillColor: "#ffefebef"
			qm_TextColor: "#00000000"
			qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
			qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
			qm_ValueVarTextOrientation: 0
			qm_MarginLeft: 0
			qm_MarginRight: 0
			qm_MarginBottom: 0
			qm_MarginTop: 0
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_Streached :false
			qm_GraphicImageID :52
			qm_ContentVisibility : false
		}
		IGuiGraphicButton
		{
			id: q486539301
			objId: 486539301
			x: 190
			y: 262
			width: 56
			height: 40
			qm_FillRectWidth: 55
			qm_FillRectHeight: 39
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_BorderColor: "#ff9c9aa5"
			qm_ImageID: 47
			qm_TileTop: 15
			qm_TileBottom: 15
			qm_TileRight: 5
			qm_TileLeft: 5
			qm_FillColor: "#ffefebef"
			qm_TextColor: "#00000000"
			qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
			qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
			qm_ValueVarTextOrientation: 0
			qm_MarginLeft: 0
			qm_MarginRight: 0
			qm_MarginBottom: 0
			qm_MarginTop: 0
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_Streached :false
			qm_GraphicImageID :54
			qm_ContentVisibility : false
		}
		IGuiGraphicButton
		{
			id: q486539302
			objId: 486539302
			x: 251
			y: 262
			width: 56
			height: 40
			qm_FillRectWidth: 55
			qm_FillRectHeight: 39
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_BorderColor: "#ff9c9aa5"
			qm_ImageID: 47
			qm_TileTop: 15
			qm_TileBottom: 15
			qm_TileRight: 5
			qm_TileLeft: 5
			qm_FillColor: "#ffefebef"
			qm_TextColor: "#00000000"
			qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
			qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
			qm_ValueVarTextOrientation: 0
			qm_MarginLeft: 0
			qm_MarginRight: 0
			qm_MarginBottom: 0
			qm_MarginTop: 0
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_Streached :false
			qm_GraphicImageID :56
			qm_ContentVisibility : false
		}
		IGuiGraphicButton
		{
			id: q486539303
			objId: 486539303
			x: 312
			y: 262
			width: 56
			height: 40
			qm_FillRectWidth: 55
			qm_FillRectHeight: 39
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_BorderColor: "#ff9c9aa5"
			qm_ImageID: 47
			qm_TileTop: 15
			qm_TileBottom: 15
			qm_TileRight: 5
			qm_TileLeft: 5
			qm_FillColor: "#ffefebef"
			qm_TextColor: "#00000000"
			qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
			qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
			qm_ValueVarTextOrientation: 0
			qm_MarginLeft: 0
			qm_MarginRight: 0
			qm_MarginBottom: 0
			qm_MarginTop: 0
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_Streached :false
			qm_GraphicImageID :58
			qm_ContentVisibility : false
		}
		IGuiGraphicButton
		{
			id: q486539304
			objId: 486539304
			x: 415
			y: 262
			width: 56
			height: 40
			qm_FillRectWidth: 55
			qm_FillRectHeight: 39
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_BorderColor: "#ff9c9aa5"
			qm_ImageID: 47
			qm_TileTop: 15
			qm_TileBottom: 15
			qm_TileRight: 5
			qm_TileLeft: 5
			qm_FillColor: "#ffefebef"
			qm_TextColor: "#00000000"
			qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
			qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
			qm_ValueVarTextOrientation: 0
			qm_MarginLeft: 0
			qm_MarginRight: 0
			qm_MarginBottom: 0
			qm_MarginTop: 0
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_Streached :false
			qm_GraphicImageID :60
			qm_ContentVisibility : false
		}
		IGuiGraphicButton
		{
			id: q486539305
			objId: 486539305
			x: 476
			y: 262
			width: 56
			height: 40
			qm_FillRectWidth: 55
			qm_FillRectHeight: 39
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_BorderColor: "#ff9c9aa5"
			qm_ImageID: 47
			qm_TileTop: 15
			qm_TileBottom: 15
			qm_TileRight: 5
			qm_TileLeft: 5
			qm_FillColor: "#ffefebef"
			qm_TextColor: "#00000000"
			qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
			qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
			qm_ValueVarTextOrientation: 0
			qm_MarginLeft: 0
			qm_MarginRight: 0
			qm_MarginBottom: 0
			qm_MarginTop: 0
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_Streached :false
			qm_GraphicImageID :61
			qm_ContentVisibility : false
		}
		IGuiGraphicButton
		{
			id: q486539306
			objId: 486539306
			x: 537
			y: 262
			width: 56
			height: 40
			qm_FillRectWidth: 55
			qm_FillRectHeight: 39
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_BorderColor: "#ff9c9aa5"
			qm_ImageID: 47
			qm_TileTop: 15
			qm_TileBottom: 15
			qm_TileRight: 5
			qm_TileLeft: 5
			qm_FillColor: "#ffefebef"
			qm_TextColor: "#00000000"
			qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
			qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
			qm_ValueVarTextOrientation: 0
			qm_MarginLeft: 0
			qm_MarginRight: 0
			qm_MarginBottom: 0
			qm_MarginTop: 0
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_Streached :false
			qm_GraphicImageID :63
			qm_ContentVisibility : false
		}
	}
	IGuiTextField
	{
		id: q268435486
		objId: 268435486
		x: 330
		y: 41
		width: 101
		height: 27
		qm_FillRectWidth: 101
		qm_FillRectHeight: 27
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
		qm_FontSize: 22
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 0
		qm_BorderColor: "#ff424952"
		qm_ImageID: 38
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 3
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
}
