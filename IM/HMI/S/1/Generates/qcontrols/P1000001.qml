﻿import QtQuick 2.0
IGuiPage
{
	id: q16777217
	objId: 16777217
	x: 0
	y: 0
	width: 800
	height: 480
	IGuiAlarmView
	{
		id: q402653184
		objId: 402653184
		x: 2
		y: 79
		width: 703
		height: 371
		qm_FillRectWidth: 702
		qm_FillRectHeight: 370
		qm_BorderCornerRadius: 4
		qm_BorderWidth: 1
		qm_BorderColor: "#ff636973"
		qm_ImageID: 43
		qm_TileTop: 6
		qm_TileBottom: 6
		qm_TileRight: 6
		qm_TileLeft: 6
		qm_FillColor: "#fff7f3f7"
		IGuiListCtrl
		{
			id: qu402653184
			objectName: "qu402653184"
			x: 2
			y: 2
			width: 699
			height: 308
			totalColumnWidth: 697
			qm_leftImageID: 44
			qm_leftTileTop: 4
			qm_leftTileBottom: 10
			qm_leftTileRight: 2
			qm_leftTileLeft: 4
			qm_middleImageID: 45
			qm_middleTileTop: 2
			qm_middleTileBottom: 12
			qm_middleTileRight: 2
			qm_middleTileLeft: 2
			qm_rightImageID: 46
			qm_rightTileTop: 4
			qm_rightTileBottom: 10
			qm_rightTileRight: 4
			qm_rightTileLeft: 2
			qm_leftBorderCornerRadius: 2
			qm_tableBackColor: "#ffffffff"
			qm_tableSelectBackColor: "#ff94b6e7"
			qm_tableAlternateBackColor: "#ffe7e7ef"
			qm_tableHeaderBackColor: "#ff84868c"
			qm_tableTextColor: "#ff181c31"
			qm_tableSelectTextColor: "#ff181c31"
			qm_tableAlternateTextColor: "#ff181c31"
			qm_tableMarginLeft: 2
			qm_tableMarginRight: 1
			qm_tableMarginBottom: 1
			qm_tableMarginTop: 1
			qm_tableHeaderTextColor: "#ffffffff"
			qm_tableHeaderValueVarTextAlignmentHorizontal: Text.AlignLeft
			qm_tableHeaderValueVarTextAlignmentVertical: Text.AlignVCenter
			qm_tableHeaderValueVarTextOrientation: 0
			qm_tableHeaderMarginLeft: 3
			qm_tableHeaderMarginRight: 1
			qm_tableHeaderMarginBottom: 1
			qm_tableHeaderMarginTop: 1
			qm_gridLineStyle: 0
			qm_gridLineWidth: 1
			qm_gridLineColor: "#ffffffff"
			qm_noOfColumns: 6
			qm_tableRowHeight: 13
			qm_tableHeaderHeight: 12
			qm_hasHeader: true
			qm_hasGridLines: true
			qm_hasBorder: false
			qm_hasDisplayFocusLine: true
			qm_hasVerticalScrolling: true
			qm_hasVerticalScrollBar: true
			qm_hasHorizontalScrollBar: false
			qm_hasColumnOrdering: false
			qm_hasHighLightFullRow: true
			qm_hasVerUpDownPresent: false
			qm_hasVerPgUpDownPresent: false
			qm_hasHighlight: true
			qm_hasUpDownAsPageUpDown: false
			qm_hasLongAlarmButton: true
			qm_hasExtraPixelForLineHeight: false
			qm_hasRowEditable: false
			qm_hasRowJustification: false
			qm_hasRowJustificationBottom: false
			qm_linesPerRow: 1
			IGuiListColumnView
			{
				id: qc118000000
				colIndex: 0
				x: 0
				y: 0
				width: 24
				height: 276
				columnType: 0
				qm_columnValueVarTextAlignmentHorizontal: Text.AlignLeft
				qm_columnValueVarTextAlignmentVertical: Text.AlignVCenter
			}
			IGuiListColumnView
			{
				id: qc218000000
				colIndex: 1
				x: 24
				y: 0
				width: 60
				height: 276
				columnType: 0
				qm_columnValueVarTextAlignmentHorizontal: Text.AlignLeft
				qm_columnValueVarTextAlignmentVertical: Text.AlignVCenter
			}
			IGuiListColumnView
			{
				id: qc318000000
				colIndex: 2
				x: 84
				y: 0
				width: 60
				height: 276
				columnType: 0
				qm_columnValueVarTextAlignmentHorizontal: Text.AlignLeft
				qm_columnValueVarTextAlignmentVertical: Text.AlignVCenter
			}
			IGuiListColumnView
			{
				id: qc418000000
				colIndex: 3
				x: 144
				y: 0
				width: 72
				height: 276
				columnType: 0
				qm_columnValueVarTextAlignmentHorizontal: Text.AlignLeft
				qm_columnValueVarTextAlignmentVertical: Text.AlignVCenter
			}
			IGuiListColumnView
			{
				id: qc518000000
				colIndex: 4
				x: 216
				y: 0
				width: 36
				height: 276
				columnType: 0
				qm_columnValueVarTextAlignmentHorizontal: Text.AlignLeft
				qm_columnValueVarTextAlignmentVertical: Text.AlignVCenter
			}
			IGuiListColumnView
			{
				id: qc618000000
				colIndex: 5
				x: 252
				y: 0
				width: 445
				height: 276
				columnType: 0
				qm_columnValueVarTextAlignmentHorizontal: Text.AlignLeft
				qm_columnValueVarTextAlignmentVertical: Text.AlignVCenter
			}
		}
		IGuiGraphicButton
		{
			id: q486539322
			objId: 486539322
			x: 4
			y: 327
			width: 56
			height: 40
			qm_FillRectWidth: 55
			qm_FillRectHeight: 39
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_BorderColor: "#ff9c9aa5"
			qm_ImageID: 47
			qm_TileTop: 15
			qm_TileBottom: 15
			qm_TileRight: 5
			qm_TileLeft: 5
			qm_FillColor: "#ffefebef"
			qm_TextColor: "#ff000000"
			qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
			qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
			qm_ValueVarTextOrientation: 0
			qm_MarginLeft: 2
			qm_MarginRight: 1
			qm_MarginBottom: 1
			qm_MarginTop: 1
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_Streached :false
			qm_GraphicImageID :84
			qm_ContentVisibility : false
		}
		IGuiGraphicButton
		{
			id: q486539298
			objId: 486539298
			x: 643
			y: 327
			width: 56
			height: 40
			qm_FillRectWidth: 55
			qm_FillRectHeight: 39
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_BorderColor: "#ff9c9aa5"
			qm_ImageID: 47
			qm_TileTop: 15
			qm_TileBottom: 15
			qm_TileRight: 5
			qm_TileLeft: 5
			qm_FillColor: "#ffefebef"
			qm_TextColor: "#ff000000"
			qm_ValueVarTextAlignmentHorizontal: Image.AlignHCenter
			qm_ValueVarTextAlignmentVertical: Image.AlignVCenter
			qm_ValueVarTextOrientation: 0
			qm_MarginLeft: 2
			qm_MarginRight: 1
			qm_MarginBottom: 1
			qm_MarginTop: 1
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_Streached :false
			qm_GraphicImageID :42
			qm_ContentVisibility : false
		}
	}
	IGuiTextField
	{
		id: q268435474
		objId: 268435474
		x: 305
		y: 34
		width: 149
		height: 27
		qm_FillRectWidth: 149
		qm_FillRectHeight: 27
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
		qm_FontSize: 22
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 0
		qm_BorderColor: "#ff424952"
		qm_ImageID: 38
		qm_TileTop: 0
		qm_TileBottom: 0
		qm_TileRight: 0
		qm_TileLeft: 0
		qm_Transparent : true 
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignLeft
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 3
		qm_MarginRight: 2
		qm_MarginBottom: 2
		qm_MarginTop: 2
	}
	IGuiButton
	{
		id: q486539323
		objId: 486539323
		x: 330
		y: 406
		width: 96
		height: 32
		qm_FillRectWidth: 95
		qm_FillRectHeight: 31
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_BorderColor: "#ff424952"
		qm_ImageID: 85
		qm_TileTop: 15
		qm_TileBottom: 15
		qm_TileRight: 5
		qm_TileLeft: 5
		qm_FillColor: "#ff636573"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_ValueVarTextOrientation: 0
		qm_MarginLeft: 0
		qm_MarginRight: 0
		qm_MarginBottom: 0
		qm_MarginTop: 0
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_FontSize: 16
		qm_FontFamilyName: "SimSun"
		qm_FontBold: true
	}
}
